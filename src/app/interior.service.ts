import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InteriorService {
  public interiors: Interior[];
  constructor() {
    this.interiors = [
      {
        id: 1,
        name: 'Interior Minimalis',
        description: 'Lorem ipsum dolor sit amet consectetur',
        image: 'https://i0.wp.com/dekoruma.blog/wp-content/uploads/2018/07/Design-Interior-Rumah-Minimalis-2.jpg',
        cost: 38000000
      },
      {
        id: 2,
        name: 'Interior Minimalis 2',
        description: 'Lorem ipsum dolor sit amet consectetur',
        image: 'https://image.archify.com/blog/l/06-interior-eksterior-desain-rumah-minimalis.jpg',
        cost: 25000000
      },
      {
        id: 3,
        name: 'Interior Modern',
        description: 'Lorem ipsum dolor sit amet consectetur',
        image: 'https://interiordesign.id/wp-content/uploads/2017/04/moderninteriorpinterest.jpg',
        cost: 10000000
      },
      {
        id: 4,
        name: 'Interior Modern 2',
        description: 'Lorem ipsum dolor sit amet consectetur',
        image: 'https://i.pinimg.com/originals/d7/9a/08/d79a08f0c0f0e26dc960695470824495.jpg',
        cost: 30000000
      },
    ];
  }

  getInteriors() {
    return this.interiors;
  }

  getInteriorById(id: number) {
    return this.interiors.find((interior) => {
      return (interior.id === id);
    })
  }
}

export interface Interior {
  id: number;
  name: string,
  description: string,
  image: string,
  cost: number
}