import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {
  public properties: Property[];
  constructor() { 
    this.properties = [
      {
        id : 1,
        name : 'Property Mezannin 2 Lantai',
        image : 'https://asset-a.grid.id/crop/0x0:0x0/700x0/photo/2018/05/25/43206018.jpg',
        description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin sapien ultrices ipsum molestie, a pellentesque nisi mattis. Donec porttitor, leo rhoncus dictum malesuada, odio massa molestie sapien, ultricies faucibus enim tellus nec magna. Nullam imperdiet, libero id congue sagittis, nisl justo blandit libero, at ultrices felis est venenatis leo. Integer gravida, urna in convallis cursus, turpis ligula eleifend nisl, id molestie diam ex a enim.r',
        location : 'Bogor, Indonesia'
      },
      {
        id : 2,
        name : 'Rumah Modern Tipe 36/40',
        image : 'https://1.bp.blogspot.com/--ZkCzhH4MSk/WdiRZihTUJI/AAAAAAAALFs/N2J7sjbaxBUkueW-kXAy8K7KCuIY0mQYACLcBGAs/s1600/2.Cara-Sederhana-Merenovasi-Rumah1.jpg',
        description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin sapien ultrices ipsum molestie, a pellentesque nisi mattis. Donec porttitor, leo rhoncus dictum malesuada, odio massa molestie sapien, ultricies faucibus enim tellus nec magna. Nullam imperdiet, libero id congue sagittis, nisl justo blandit libero, at ultrices felis est venenatis leo. Integer gravida, urna in convallis cursus, turpis ligula eleifend nisl, id molestie diam ex a enim.',
        location : 'DKI Jakarta, Indonesia'
      },
      {
        id : 3,
        name : 'Rumah Minimalis Tipe 21/24',
        image : 'https://blogpictures.99.co/rumah-type-21-5.jpg',
        description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin sapien ultrices ipsum molestie, a pellentesque nisi mattis. Donec porttitor, leo rhoncus dictum malesuada, odio massa molestie sapien, ultricies faucibus enim tellus nec magna. Nullam imperdiet, libero id congue sagittis, nisl justo blandit libero, at ultrices felis est venenatis leo. Integer gravida, urna in convallis cursus, turpis ligula eleifend nisl, id molestie diam ex a enim.',
        location : 'Tasikmalaya, Indonesia'
      },
      {
        id : 4,
        name : 'Rumah Minimalis Tipe 45',
        image : 'https://insinyurbangunan.com/wp-content/uploads/2018/11/featured-image.jpg',
        description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin sapien ultrices ipsum molestie, a pellentesque nisi mattis. Donec porttitor, leo rhoncus dictum malesuada, odio massa molestie sapien, ultricies faucibus enim tellus nec magna. Nullam imperdiet, libero id congue sagittis, nisl justo blandit libero, at ultrices felis est venenatis leo. Integer gravida, urna in convallis cursus, turpis ligula eleifend nisl, id molestie diam ex a enim.',
        location : 'Cianjur, Indonesia'
      },
      {
        id : 5,
        name : 'Rumah Minimalis Tipe 60',
        image : 'https://interiordesign.id/wp-content/uploads/2017/11/desain-rumah-tipe-60-3.jpg',
        description : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin sapien ultrices ipsum molestie, a pellentesque nisi mattis. Donec porttitor, leo rhoncus dictum malesuada, odio massa molestie sapien, ultricies faucibus enim tellus nec magna. Nullam imperdiet, libero id congue sagittis, nisl justo blandit libero, at ultrices felis est venenatis leo. Integer gravida, urna in convallis cursus, turpis ligula eleifend nisl, id molestie diam ex a enim.',
        location : 'Garut, Indonesia'
      },
    ]// End get properties
  }

  getProperties () {
    return this.properties;
  }

  getPropertyById(id: number) {
    return this.properties.find((property) => {
      return (property.id === id)
    });
  }
}

export interface Property {
  id: number;
  name: string;
  image: string;
  description: string;
  location: string;
}
