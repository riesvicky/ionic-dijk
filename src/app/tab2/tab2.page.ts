import { Component } from '@angular/core';
import { Interior, InteriorService } from '../interior.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  public interiors: Interior[];
  public loadedInteriors: Interior[];

  constructor(private _interiorService: InteriorService, private router: Router) {}

  ngOnInit() {
    this.interiors = this._interiorService.getInteriors();
    this.loadedInteriors = this.interiors;
    this.initializedItems()
  }

  ionViewDidEnter() {
    this.setStyleToItems()
  }

  async initializedItems() {
    this.interiors = this.loadedInteriors;
  }

  setStyleToItems() {
    const shadow = 'box-shadow: 0 0.46875rem 2.1875rem rgba(4,9,20,0.03)'; 

    document.querySelectorAll('app-tab2 .item').forEach((item, index) => {
      item.shadowRoot.querySelector('.item-inner').setAttribute('style', 'border-bottom:0');

      item.shadowRoot.querySelector('.item-native').setAttribute('style', 'padding: 8px; border-radius: 6px; background-color: #FFF;' + shadow)
    });
    
    console.log(document.querySelectorAll('.item')); 
  }

  onSelect(interior) {
    this.router.navigate(['tabs/tab2/interiors', interior.id]);
  }

  async filterItems(event: any) {
    const val: string= event.target.value;
    await this.initializedItems();

    if(!val) {
      setTimeout(() => {
        this.setStyleToItems();
      }, 0)
      return;
    }

    this.interiors = this.interiors.filter(interior => {
      if (interior.name.toLowerCase().indexOf(val.toLowerCase()) > -1 ){
        return true
      }
      return false
    })  

    setTimeout(() => {
      this.setStyleToItems();
    }, 0)
      
  }
}
