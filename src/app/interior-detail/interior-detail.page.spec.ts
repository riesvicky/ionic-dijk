import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InteriorDetailPage } from './interior-detail.page';

describe('InteriorDetailPage', () => {
  let component: InteriorDetailPage;
  let fixture: ComponentFixture<InteriorDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InteriorDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InteriorDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
