import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InteriorDetailPageRoutingModule } from './interior-detail-routing.module';

import { InteriorDetailPage } from './interior-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InteriorDetailPageRoutingModule
  ],
  declarations: [InteriorDetailPage]
})
export class InteriorDetailPageModule {}
