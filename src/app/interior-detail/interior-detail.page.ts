import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InteriorService, Interior } from '../interior.service';

@Component({
  selector: 'app-interior-detail',
  templateUrl: './interior-detail.page.html',
  styleUrls: ['./interior-detail.page.scss'],
})
export class InteriorDetailPage implements OnInit {
  public interiorID: any;
  public interior: Interior;
  public cost: string;

  constructor(
              private interiorService: InteriorService,
              private _activatedRoute: ActivatedRoute
  ) { }
  
  /**
   * When initialized
   */
  ngOnInit() {
    this.interiorID = this._activatedRoute.snapshot.paramMap.get('id');
    this.interiorID = parseInt(this.interiorID);

    this.interior = this.interiorService.getInteriorById(this.interiorID);

    this.cost = new Intl.NumberFormat('id-ID', {style: 'currency', currency: 'IDR'}).format(this.interior.cost);
  }

}
