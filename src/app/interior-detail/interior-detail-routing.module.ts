import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InteriorDetailPage } from './interior-detail.page';

const routes: Routes = [
  {
    path: '',
    component: InteriorDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InteriorDetailPageRoutingModule {}
