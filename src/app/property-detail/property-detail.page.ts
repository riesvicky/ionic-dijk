import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PropertyService } from '../property.service';

@Component({
  selector: 'app-property-detail',
  templateUrl: './property-detail.page.html',
  styleUrls: ['./property-detail.page.scss'],
})
export class PropertyDetailPage implements OnInit {
  public propertyID;
  public property;

  constructor(private _propertyService: PropertyService, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.propertyID = this._activatedRoute.snapshot.paramMap.get('id');
    this.propertyID = parseInt(this.propertyID);
    
    this.property = this._propertyService.getPropertyById(this.propertyID);
  }

}
