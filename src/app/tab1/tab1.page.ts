import { Component } from '@angular/core';
import { PropertyService } from '../property.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public properties = [];
  public loadedProperties = [];

  constructor(
    private router: Router, 
    private _propertyService: PropertyService
    ) {}

  ngOnInit() {
    this.properties = this._propertyService.getProperties();
    this.loadedProperties = this.properties;
    this.initializedItems()
  }

  ionViewDidEnter() {
    this.setStyleToItems()
  }

  onSelect(property) {
    this.router.navigate(['tabs/tab1/properties', property.id])
  }

  initializedItems(): void {
    this.properties = this.loadedProperties;

    return;
  }

  setStyleToItems() {
    const shadow = 'box-shadow: 0 0.46875rem 2.1875rem rgba(4,9,20,0.03)'; 

    document.querySelectorAll('app-tab1 .item').forEach((item, index) => {
      item.shadowRoot.querySelector('.item-inner').setAttribute('style', 'border-bottom:0');

      item.shadowRoot.querySelector('.item-native').setAttribute('style', 'padding: 8px; border-radius: 6px; background-color: #FFF;' + shadow)
    });
    
    console.log(document.querySelectorAll('.item')); 
  }

  filterItems(event: any) {
    const val: string = event.target.value;
    this.initializedItems();

    if(!val) {
      setTimeout(() => {
        this.setStyleToItems();
      }, 0)
      return;
    }

    this.properties = this.properties.filter(property => {
      if(property.name.toLowerCase().indexOf(val.toLowerCase()) > -1){
        return true
      }
      return false
    })

    setTimeout(() => {
      this.setStyleToItems();
    }, 0)
  }

}
