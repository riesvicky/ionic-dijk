import { TestBed } from '@angular/core/testing';

import { InteriorService } from './interior.service';

describe('InteriorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InteriorService = TestBed.get(InteriorService);
    expect(service).toBeTruthy();
  });
});
