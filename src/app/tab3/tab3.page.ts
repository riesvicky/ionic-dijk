import { Component } from '@angular/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  public author: Author;
  constructor() {
    this.author = {
      avatar: 'https://bit.ly/39Xw2EQ',
      name: 'Diky Rizky A.Md',
      job: 'BPJS Bogor',
      bio: 'Seorang mantan perawat yang banting stir',
      expertises: ['Angular', 'Ionic', 'SASS/LESS', 'React JS', 'React Native', 'MongoDB']
    }
  }

  ngOnInit() {
    
  }
}

export interface Author {
  avatar: string;
  name: string;
  job: string;
  bio: string;
  expertises: string[]
}
